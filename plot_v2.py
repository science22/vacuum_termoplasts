#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 13 13:51:14 2022

@author: pete
"""

import pandas as pd
import math 
from math import isnan
from datetime import datetime
import numpy as np

#from matplotlib import plot,gca

empthy = pd.read_csv('20221031132102.csv',';')
bb = pd.read_csv('20221114105830.csv',';')
wire = pd.read_csv('20221115103753.csv',';')

times = [frames['UNIX_timestamp(sec)'].to_numpy().astype('datetime64[s]')]

plot(time,frames['Pressure(mbar)'])
ax = gca()
ax.set_yscale('log')
ylim(1e-5,10)

from lmfit import minimize, Parameters, report_fit

colors = {"em":'k',"PLA":"r","PC":"c","PA12":"b","TPU":"gray","PA6":"m","ABS":"orange","PET":"g"}
markers = {"infi":'-x',' fill':"-o"}

def fitfunc(pv,x3):
    b = pv['b']
    x0 = pv['x0']
    k = pv['k']
    a = pv['a']

    return a*(1-np.exp(-k*((x3-x0))))+b

def res(par,x,data):
    pv = par.valuesdict()
    y = fitfunc(pv, x)
    return (abs(data-y))/x
    
prs = Parameters()
prs.add('b',0.0028)
prs.add('x0',0.001)
prs.add('k',8e-4)
prs.add('a',0.69)

x1 = ((base_1.iloc[:,1]-base_1.iloc[vmin_1,1]).astype('int')/1e9/60).to_list()
x2 = ((base_2.iloc[:,1]-base_2.iloc[vmin_2,1]).astype('int')/1e9/60).to_list()


x = array(x1+x2)


dat = base_1['Pressure'].to_list()+ base_2['Pressure'].to_list()
dat = array(dat)

inx = np.logical_or(np.isnan(dat), x<0)

dat = np.delete(dat,inx)
x = np.delete(x,inx)
x[x==0]=0.5

mini_e = minimize(res, prs, "nelder",args = (x,dat))
report_fit(mini_e)

pv = mini_e.params.valuesdict()
b = pv['b']
x0 = pv['x0']
k = pv['k']
a = pv['a']


x3 = linspace(1,2000,2000)
y = fitfunc(pv, x3)

resd = res(mini_e.params,x,dat)

close('all')
plot(x1,base_1['Pressure'],'x-k',label="Empthy, p1")
plot(x2,base_2['Pressure'],'o-k',label="Empthy, p2")

plot(x3,y,'-r',label=f'{a:0.3f}*exp(-{k:0.3f}*(x-{x0:0.3f}))+{b:0.3f}')
plot(x,resd,'xk',label=f'exp residual',linewidth=0.5)

prs = Parameters()
prs.add('b',0.08)
prs.add('x0',1e-4)
prs.add('k',1e-7)
prs.add('a',1e-11)


def fitfunc_p(pv,x):
    b = pv['b']
    x0 = pv['x0']
    k = pv['k']
    a = pv['a']
    return b+x0*x+k*x**2++a*x**3

def res_p(par,x,data):
    pv = par.valuesdict()
    y = fitfunc_p(pv, x)
    return data-y
    

mini = minimize(res_p, prs, "nelder",args = (x,dat))
report_fit(mini)

pv = mini.params.valuesdict()
b = pv['b']
x0 = pv['x0']
k = pv['k']
a = pv['a']


x3 = linspace(0.5,2000,2000)
y = fitfunc_p(pv, x3)

plot(x3,y,'.-b',label='poly$^4$')
legend()

loglog()
ylim(1e-5,1.1)
xlabel("Time, min")
ylabel("Pressure, mBar")

#savefig('empthy.png')

figure()

for fr in frames:
    if "emp" in fr:
        continue
    df = frames[fr]
    vmin = np.where(df.iloc[:,2] == df.iloc[:,2].min())[0][-1]
    clr = None
    for c in colors:
        if c in fr:
            clr = colors[c]
            break
            
    mrk = "-"
    for m in markers:
        if m in fr:
            mrk = markers[m]
    x = (df.iloc[:,1]-df.iloc[vmin,1]).astype('int')/1e9/60
    plot(x,df['Pressure']-fitfunc(mini_e.params.valuesdict(),x),mrk,label=fr,color=clr)
    
ax = gca()
ax.set_xscale('log')
ax.set_yscale('log')
xlabel("Time,m")
ylabel("Pressure excess, mBar")
legend()

savefig('subtracted_log.png')