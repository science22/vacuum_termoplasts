#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 13 13:51:14 2022

@author: pete
"""

import pandas as pd
import math 
from math import isnan
from datetime import datetime
import numpy as np
frames = pd.read_excel('experiments.xlsx',sheet_name=None)


with pd.ExcelWriter('out.xlsx',mode='w') as writer:  
    for fr in frames:
        df = frames[fr]
        df = df[2:]
        data = df[df.columns[2]]
        
        values = []
        labels = []
        for v in data:
            if type(v) is float and  math.isnan(v):
                values.append(float("nan"))
            else:
                char = '-'
                if not char in v:
                    char = '–'
                number = float(v[:v.index(char)].strip()+"e-"+v[v.index(char)+1:].strip())
                values.append(number)
        
        date = df.iloc[0,0]
        date = datetime.strptime(date,"%d.%m.%y")
        times = []
        
        for i in range(df.shape[0]):
            v = df.iloc[i,0]
            if not(type(v) is float and  math.isnan(v)) and len(v.strip())!=0:
                date = datetime.strptime(v,"%d.%m.%y")
            datet = df.iloc[i,1]
            if type(datet) is float and (isnan(values[i]) and isnan(datet)):
                values.pop(i)
                continue
            cdate = datetime(date.year,date.month,date.day,datet.hour,datet.minute)
            times.append(cdate)
            lab = df.iloc[i,3]
            if (not(type(lab) is float and  math.isnan(lab)) and len(lab.strip())!=0):
                labels.append(lab)
            else: 
                labels.append('')
            
        values = np.array(values)
        times = np.array(times)
        labels = np.array(labels)
        
        
        out = pd.DataFrame({'Time':times,'Pressure':values,"Labels":labels})
        out.to_excel(writer,sheet_name=fr)
