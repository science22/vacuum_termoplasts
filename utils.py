#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  9 16:21:22 2022

@author: edombek
"""

import numpy as np

#short names
p = 'Pressure(mbar)'
d = 'datetime'
dd = 'deltatime'

def fitfunc(pv,x):
    b = pv['b']
    x0 = pv.get('x0',0)
    k = pv['k']
    a = pv['a']

    return a*(1-np.exp(-k*((x-x0))))+b

def residual(par,x,data):
    pv = par.valuesdict()
    y = fitfunc(pv, x)
    return (np.abs(data-y))/x