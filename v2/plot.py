#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 13 13:51:14 2022

@author: pete
"""

import pandas as pd
import math 
from math import isnan
from datetime import datetime
import numpy as np
from lmfit import minimize, Parameters, report_fit

#from matplotlib import plot,gca

frames = pd.read_excel('out.xlsx',sheet_name=None)
colors = {"em":'k',"Em":'k',"PLA":"r","PC":"c","PA12":"b","TPU":"gray","PA6":"m","ABS":"orange","PET":"g"}
markers = {"infi":'-x',' fill':"-o"}
markers = {"Em":'.-','PA':"-o",'ABS':"-x"}

base_1 = frames['Empthy']
vmin_1 = 8

base_2 = frames['Empthy_2']
vmin_2 = 5

base_3 = frames['Empthy_3']
vmin_3 = 15


x1 = array(((base_1.iloc[:,1]-base_1.iloc[vmin_1,1]).astype('int')/1e9/60+0.001).to_list())
x2 = ((base_2.iloc[:,1]-base_2.iloc[vmin_2,1]).astype('int')/1e9/60+0.001)

x2[16:20]=nan
x2[20:]-=x2[20]+0.001

x2= array(x2.to_list())

x3 = array(((base_3.iloc[:,1]-base_3.iloc[vmin_3,1]).astype('int')/1e9/60+0.001).to_list())

def fitfunc(pv,x3):
    b = pv['b']
    x0 = pv['x0']
    k = pv['k']
    a = pv['a']

    return a*(1-np.exp(-k*((x3-x0))))+b

params = Parameters().load(open('../empthy.prs','r'))
pv = params.valuesdict()
b = pv['b']
x0 = pv['x0']
k = pv['k']
a = pv['a']

xy = linspace(1,2000,2000)
y = fitfunc(pv, xy)


close('all')
plot(x1,base_1['Pressure'],'x-k',label=f"{base_1.iloc[0,1].date()}")
plot(x2,base_2['Pressure'],'o-k',label=f"{base_2.iloc[0,1].date()}")
plot(x2[19:],base_2['Pressure'][19:],'or',label=f"{base_2.iloc[21,1].date()} second")
plot(x3[vmin_3:],base_3['Pressure'][vmin_3:],'.-k',label=f"{base_3.iloc[0,1].date()}, w/heat")
plot(xy,y,'-r',label=f'Fit')


legend()

loglog()
ylim(1e-3,2)
xlabel("Time, min")
ylabel("Pressure, mBar")
xlim(0.5,4000)

figure()

plot(x1,base_1['Pressure']-fitfunc(params.valuesdict(),x1),'x-k',label=f"{base_1.iloc[0,1].date()}")
plot(x2,base_2['Pressure']-fitfunc(params.valuesdict(),x2),'o-k',label=base_2.iloc[0,1].date())
plot(x2[19:],(base_2['Pressure']-fitfunc(params.valuesdict(),x2))[19:],'or',label=f"{base_2.iloc[21,1].date()} second")
plot(x3,base_3['Pressure']-fitfunc(params.valuesdict(),x3),'.-k',label=f"{base_2.iloc[0,1].date()} w/heat")

xlabel("Time, min")
ylabel("Pressure excess, mBar")

ax = gca()
ax.set_xscale('log')
ax.set_yscale('symlog',linthresh=0.01)
legend()
xlim(0.5,4000)
#yticks([-0.1,0,0.05,0.1,0.5,1])

figure()
for fr in frames:
    if "emp" in fr or ("Emp" in fr and not "_3" in fr ):
        continue

    df = frames[fr]
    x = (df.iloc[:,1]).astype('int')/1e9/60
    x-=x[0]
    start = 0
    stop = 0
    for i in range(start,df.shape[0]):
        if df['Labels'][i] in ['выкл. ТМН','Выкл. ТМН','Выкл.ТМН']:
            start = i
            print(f'-{stop}:{start}')
            x[stop:start]=nan
        elif df['Labels'][i] in ['вкл. ТМН','Вкл. ТМН']:
            stop = i
            print(f'+{start}:{stop}')
            x[start:stop] -= x[start]
        
    clr = None
    for c in colors:
        if c in fr:
            clr = colors[c]
            break
            
    mrk = "-"
    for m in markers:
        if m in fr:
            mrk = markers[m]
    #plot(x,df['Pressure'],mrk,label=fr,color=clr)
    plot(x,df['Pressure']-fitfunc(params.valuesdict(),x),mrk,label=fr,color=clr)

#plot(xy,y,'-r',label=f'{a:0.3f}*exp(-{k:0.3f}*(x-{x0:0.3f}))+{b:0.3f}')
    
ax = gca()
ax.set_xscale('log')
ax.set_yscale('symlog',linthresh=0.01)
xlabel("Time, min")
ylabel("Pressure excess, mBar")
legend()
#yticks([-0.015,0,0.01,0.02,0.05])
#yticks([-0.1,0,0.05,0.1,0.5,1])

savefig('plastic_base.png')