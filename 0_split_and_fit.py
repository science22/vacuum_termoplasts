#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  7 13:12:14 2022

@author: edombek
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from lmfit import minimize, Parameters, report_fit
from utils import fitfunc, residual, p, d, dd

save_prefix = 'v3/1'
df = pd.read_csv('v3/20221031132102.csv',';')
df[d] = pd.to_datetime(df['UNIX_timestamp(sec)'],unit='s')
df = df[[d,p]]

if 0:
    plt.figure()
    plt.plot(df[d], df[p])
    plt.yscale('log')
    plt.tight_layout()
    plt.show()

intervals=[('2022-10-31 13:25:25', '2022-11-01 6:41:51'),
           ('2022-11-01 10:48:00', '2022-11-02 5:00:00'),
           ('2022-11-02 12:25:23', '2022-11-03 9:40:53')]

#split
dfs = []
for interval in intervals:
    start, end = pd.to_datetime(interval)
    dfs.append(df[(df[d] > start) & (df[d] < end)])
if 0:
    for df in dfs:
        plt.figure()
        plt.plot(df[d], df[p])
        plt.yscale('log')
        plt.tight_layout()
        plt.show()

for i, df in enumerate(dfs):
    df[dd] = (df[d]-df[d].iloc[0]).dt.total_seconds()
    
    prs = Parameters()
    prs.add('b',0.0028)
    prs.add('x0',0.001)
    prs.add('k',8e-4)
    prs.add('a',0.69)
    
    x = df[dd].to_numpy()[1:]
    y = df[p].to_numpy()[1:]
    
    mini = minimize(residual, prs, "nelder",args = (x,y))
    report_fit(mini)
    pv = mini.params.valuesdict()
    b = pv['b']
    x0 = pv['x0']
    k = pv['k']
    a = pv['a']
    
    if 1:
        x_ = np.logspace(0,np.log10(x[-1]),4096)
        y_ = fitfunc(pv, x_)
        plt.figure()
        plt.plot(x,y,'r', label='data')
        plt.plot(x_,y_,'k--', label='fit')
        plt.yscale('log')
        plt.tight_layout()
        plt.show()
    
    df[dd] -= pv['x0']
    pv.pop('x0')
    df_par = pd.DataFrame()
    for n, val in pv.items():
        df_par[n] = [val]
    df.to_csv(f'{save_prefix}_{i}.csv')
    df_par.to_csv(f'{save_prefix}_{i}_params.csv')