#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  9 16:05:22 2022

@author: edombek
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import scale
from lmfit import minimize, Parameters, report_fit
from utils import fitfunc, p, dd

def read(prefix, i):
    return pd.read_csv(f'{prefix}_{i}.csv'), pd.read_csv(f'{prefix}_{i}_params.csv').to_dict('records')[0]

prefix = 'v3/1'
N = 3

names = ['Пустой, а', 'Пустой, б', 'Образец чёрного тела'] # нагретый
tipes = ['--', '-.', ':']
referi = 1

dats = []
for i in range(N):
    dats.append(read(prefix, i))

refer = dats[i]

s=25
fig, ax = plt.subplots(1,1)
ax.set_xscale('log')
ax.set_yscale(scale.SymmetricalLogScale(ax, linthresh=1e-2))
#ax.grid(which='major', color = 'k', linewidth = 0.75)
#ax.grid(which='minor', color = 'k', linewidth = 0.25)
for i, dat in enumerate(dats):
    ax.plot(dat[0][dd]/60, dat[0][p].rolling(s, center=True, method='single').mean(), tipes[i], label=names[i])
ax.plot(dat[0][dd]/60, (dat[0][p]-dats[-2][0][p]).rolling(s, center=True).mean(), label='Разность')
#ax.plot(dat[0][dd]/60, dat[0][p]-fitfunc(dat[1], dat[0][dd]), label=names[i]+' - Пустой, 1.1')
ax.set_xlim(.5)
ax.legend()
ax.set_xlabel('Время, мин')
ax.set_ylabel('Давление, мБар')
fig.tight_layout()       
fig.savefig('figure5.pdf')